using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareTags : MonoBehaviour
{
    public enum TagCompare{
        Tag,
        CompareTag
    }

    public enum Reference
    {
        gameObject,
        Obj
    }

    public GameObject obj;
    public int count;

    public TagCompare _currentTagType;
    public Reference _currentReference;

    // Start is called before the first frame update
    void Start()
    {
        obj = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        for(int i = 0; i < count; i++)
        {
            if(_currentTagType == TagCompare.CompareTag)
            {
                if(_currentReference == Reference.gameObject)
                {
                    if (gameObject.CompareTag("Player")) { }
                }
                else if(_currentReference == Reference.Obj)
                {
                    if (obj.CompareTag("Player")) { }
                } 
            }
            else if (_currentTagType == TagCompare.Tag)
            {
                if (_currentReference == Reference.gameObject)
                {
                    if (gameObject.tag == "Player") { }
                }
                else if (_currentReference == Reference.Obj)
                {
                    if (obj.tag == "Player") { }
                }
            }
        }
    }
}
