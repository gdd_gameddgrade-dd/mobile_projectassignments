﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {

	private void OnGUI()
	{
		GUI.Label(new Rect(0, 0, 500, 50), "Current Scene: " + SceneManager.GetActiveScene().name);
	}

	private void Update () 
	{
		if (Input.anyKeyDown || Input.GetMouseButton(0))
		{
			var currentScene = SceneManager.GetActiveScene().buildIndex;

			/*
			if (currentScene == SceneManager.sceneCountInBuildSettings - 1)
				Application.Quit();
			else
				SceneManager.LoadScene(currentScene + 1);
			*/

			SceneManager.LoadScene((currentScene + 1) % SceneManager.sceneCount);
			//SceneManager.LoadScene((Application.loadedLevel + 1) % Application.levelCount);
		}
	}
}
