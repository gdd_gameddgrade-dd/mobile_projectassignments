using UnityEngine;

namespace Laboratory.Scripts.Lab6
{
    public class GameManagement : MonoBehaviour
    {
        public GameObject[] _prefabAnimalList;
        public float countDown = 0;
        public float DelayCountDown = 1;

        [Header("Force Direction Scale")]
        public float upForce = 1f;
        public float sideForce = 0.1f;

        public int _score = 0;
        public static GameManagement singleton;
        
        [Header("Pooling")]
        public int LinkCount = 0;
        public int LimitSpwan = 100;

        public enum InstanceMode {
            Instance,
            Pool
        }
        public InstanceMode instanceMode = InstanceMode.Instance;
        // Start is called before the first frame update
        void Start() {
            countDown = DelayCountDown;
            singleton = this;
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                SpawnAnimal();
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                Lean.Pool.LeanPool.DespawnAll();
            }
        }

        public void FixedUpdate() {
            countDown -= Time.deltaTime;
            if(countDown < 0 && LinkCount < LimitSpwan)
                SpawnAnimal();
        }
        void SpawnAnimal() {
            countDown = DelayCountDown;
            int indexAnimal = Random.Range(0, _prefabAnimalList.Length);
            GameObject Obj = InstantiateObject(_prefabAnimalList[indexAnimal]);
            float xForce = Random.Range(-sideForce, sideForce);
            float yForce = Random.Range(upForce / 2f, upForce);
            Obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(xForce, yForce), ForceMode2D.Impulse);
        }
        GameObject InstantiateObject(GameObject obj) {
            if(instanceMode == InstanceMode.Instance)
                return Instantiate(obj, this.transform.position, Quaternion.identity);
            else if(instanceMode == InstanceMode.Pool)
                return Lean.Pool.LeanPool.Spawn(obj, this.transform.position, Quaternion.identity);
            return null;
        }
        public void PlusScore() {
            _score++;
        }

    }
}