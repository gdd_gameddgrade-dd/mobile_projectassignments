using UnityEngine;

namespace Laboratory.Scripts.Lab6
{
    public class SelfDespawnSelfDespawn : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag.Contains("Touch"))
            {
                DestoryGameObject();
                GameManagement.singleton.PlusScore();
            }
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (transform.position.y < -10)
                DestoryGameObject();
        }

        void DestoryGameObject()
        {
            if (GameManagement.singleton.instanceMode == GameManagement.InstanceMode.Pool)
                Lean.Pool.LeanPool.Despawn(this.gameObject);
            else Destroy(this.gameObject);
        }
    }
}