using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoneCacheUpdateManager : MonoBehaviour
{

    private ManagedUpdateBehavior[] list;


	private void Update()
	{
		list = GetComponents<ManagedUpdateBehavior>();

		if (list == null)
			return;

		var count = list.Length;
		for (var i = 0; i < count; i++)
		{
			list[i].UpdateMe();
		}
	}
}
